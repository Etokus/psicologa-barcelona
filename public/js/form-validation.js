$(document).ready(function() {
  $('#form-bottom').validate({
      debug: true,

      submitHandler: function() {
          const nombre = $('input[name="nombre"]').val();
          const apellido = $('input[name="apellido"]').val();
          const telefono = $('input[name="telefono"]').val();
          const email = $('input[name="email"]').val();
          const comentarios = $('textarea[name="comentarios"]').val();

              $.ajax({
                type: "POST",
                url: '/contactado',
                data: {
                  nombre,
                  apellido,
                  telefono,
                  email,
                  comentarios
                },
                success: function() {
                  M.toast({
                    html: 'Formulario Enviado Con éxito! :) ',
                    classes: 'toast-form-success',
                  });
                },
                error: function( jqXHR, textStatus, errorThrown ) {
                  M.toast({
                    html: 'ERROR: No se ha efectuado la reserva! :( ',
                    classes: 'toast-form-error',
              });
            }
          });
      },
      rules: {
          name: {
              required: true,
              minlength: 2
          },
          apellido: {
              required: true,
              number: true,
              range: [1, 25]
          },
          email: {
              required: true,
              email: true
          },
          telefono: {
              required: true,
              phone:true
          },
          comentarios: {
              maxlength: 1000
          },
      },
      errorLabelContainer: '.errorTxt'
  })
});

$.validator.addMethod( "phone", function( phone_number ) {
    return phone_number.match(/^(\+34|0034|34)?[6|7|9][0-9]{8}$/)
}, "" );
