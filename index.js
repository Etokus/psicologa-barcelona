var express = require ("express");
var mongoose = require ("mongoose");
var jquery = require ("jquery");
var routes = require('./routes/routes');
const routesform = require('./routes/routesform');
var bodyParser = require('body-parser');
const path = require('path');
var compression = require('compression');
var helmet = require('helmet');
var sm = require('sitemap');

var app = express();


//settings
app.set("appName", "mamaweb");
app.set("views", __dirname + "/views");
app.set("view engine", "ejs");
app.use(express.static(__dirname + '/public/css'));
app.use(express.static(__dirname + '/public/js'));
app.use(express.static(__dirname + '/public/imagenes'));
app.use(express.static(path.resolve(__dirname, 'public')));
app.use(compression());
app.use(helmet());

//db
mongoose.connect("mongodb://mamadb:mama123456@ds113923.mlab.com:13923/mamadb", function (err) {
 if (err) throw err;
 console.log("Conectados a la base de datos");
 });

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// rutas
app.use('/', routes);
app.use("/", routesform);

//sitemap
sitemap = sm.createSitemap ({
      hostname: 'http://www.vuestropsicologo.com',
      cacheTime: 600000,
      urls: [
        { url: '/ansiedad/', priority: 0.9},
        { url: '/depresion/', priority: 0.9},
        { url: '/baja-autoestima/', priority: 0.9},
        { url: '/psicologa-mercedes-ortiz/', priority: 0.7},
        { url: '/visitas-online/', priority: 0.7},
        { url: '/contacto/', priority: 0.7},
        { url: '/psicologa-mercedes-ortiz/',priority: 0.4},
        { url: '/psicologa-mercedes-ortiz/',prioritypriority: 0.4},
      ]
    });

app.get('/sitemap.xml', function(req, res) {
  sitemap.toXML( function (err, xml) {
      if (err) {
        return res.status(500).end();
      }
      res.header('Content-Type', 'application/xml');
      res.send( xml );
  });
});

//Error 404
app.use(function(req, res){
    res.type('text/plain');
    res.status(404);
    res.render('400.ejs');
});

// Error 500
app.use(function(err, req, res, next){
    console.error(err.stack);
    res.type('text/plain');
    res.status(500);
    res.render('500.ejs');
});

//localhost:2000
app.listen(process.env.PORT || 8080, function(){
    console.log("Express server listening on port %d in %s mode", this.address().port, app.settings.env);
});
