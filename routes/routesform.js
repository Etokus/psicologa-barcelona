const express = require ("express");
const router = express.Router();
const mongoose = require("mongoose");
const validator = require('express-validator');
const nodemailer = require('nodemailer');
const path = require('path');

const app = express();

//mailing puertos
var transporter = nodemailer.createTransport({
    service: 'Mandrill',
    port: 587,
    //secure: true, //ssl
    auth: {
        user: 'vuestropsicologo',
        pass: 'iCejfRN2NGJZZBY7N19tBA'
    }
});
//Schemas
var ReservasSchema = new mongoose.Schema({
  nombre: String,
  telefono: String,
  email: String,
  comentarios: String
});

const reservaModel = mongoose.model("reserva", ReservasSchema);

router.post("/contactado", (req, res) => {
    const formulario = new reservaModel(req.body);
    formulario.save(function (err, user) {
        if (err) {
          console.log(err);
          res.status(500).json({status: err})
        }
        else {
          const mailOptions = {
                from: 'info@vuestropsicologo.com',
                to: 'albertoseor@gmail.com',
                subject: 'Formulario de contacto',
                html: `
                    <h1>Hola </h1>, <br> han hecho una reserva: <br>
                     Nombre: ${req.body.name},
                     Telefono: ${req.body.telefono},
                     Email: ${req.body.email},
                     Comentarios: ${req.body.comentarios},
                `
            };
            transporter.sendMail(mailOptions, function(error, info) {
                if (error) {
                    console.log(error);
                } else {
                    console.log('Email sent: ' + info.response);
                }
            });
          res.status(200).json({status: 'ok'})
        }
      });
});


module.exports = router
