var express = require ("express");
var router = express.Router();

router.get("/", (req, res) => {
  res.render("Index.ejs");
});

router.get("/ansiedad", (req, res) => {
  res.render("ansiedad.ejs");
});

router.get("/depresion", (req, res) => {
  res.render("depresion.ejs");
});

router.get("/baja-autoestima", (req, res) => {
  res.render("baja-autoestima.ejs");
});

router.get("/psicologa-mercedes-ortiz", (req, res) => {
  res.render("cv.ejs");
});

router.get("/contacto", (req, res) => {
  res.render("contacto.ejs");
});

router.get("/visitas-online", (req, res) => {
  res.render("visitas-online.ejs");
});

router.get("/cookies", (req, res) => {
  res.render("politica-cookies.ejs");
});

router.get("/privacidad-legal", (req, res) => {
  res.render("politica-privacidad.ejs");
});

router.get("*", function (req, res) {
  res.render("no-encontrado.ejs");
})


module.exports = router
